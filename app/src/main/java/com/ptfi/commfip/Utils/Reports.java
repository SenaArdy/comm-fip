package com.ptfi.commfip.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.CellVerticalAlignment;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.Font;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.ParagraphAlignment;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.commfip.Database.DataSource;
import com.ptfi.commfip.Models.ChecklistModel;
import com.ptfi.commfip.Models.ComplianceModel;
import com.ptfi.commfip.Models.FIPChecklistModel;
import com.ptfi.commfip.Models.FIPModel;
import com.ptfi.commfip.Models.HandoverModel;
import com.ptfi.commfip.Models.PhotoModel;
import com.ptfi.commfip.Models.TermsModel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by senaardyputra on 8/1/16.
 */
public class Reports {

    static String version;

    public static final int OPEN_PREVIEW = 323;

    static Row clonedRow;
    static String passCheck = Html.fromHtml("&#10004;").toString();

    private static ArrayList<ComplianceModel> complianceData = new ArrayList<>();
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();

    private static ArrayList<TermsModel> termsData = new ArrayList<>();

    private static ArrayList<ChecklistModel> inputData = new ArrayList<>();
    private static ArrayList<ChecklistModel> outputData = new ArrayList<>();
    private static ArrayList<ChecklistModel> interlockData = new ArrayList<>();

    public static void createCommissioningReport(final Activity mActivity, final String equipment, final String date,
                                                 final String location, final String type, final String register,
                                                 final boolean isGenerate) {
        new AsyncTask<String, Integer, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Generate Commissioning FIP Reports. Please Wait...");
                } else if (!isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Preview Commissioning FIP Reports. Please Wait...");
                }
            }

            @Override
            protected Boolean doInBackground(String... params) {

                String reportPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();

                reportPath = FoldersFilesName.TEMPLATES_FOLDER_ON_EXTERNAL_PATH + "/" + FoldersFilesName.TEMPLATE_COMMISSIONING_FIP;

                String fileName;
                String formattedID = equipment;
                String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
                String docName = time + " Commissioning Sprinkler " + formattedID;

                // CREATE IMAGE FOLDER
                String imageFolderPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/"
                        + FoldersFilesName.ROOT_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.APP_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.EXPORT_FOLDER_NAME + "/" + docName;
                File imageFolder = new File(imageFolderPath);

                try {
                    Document doc = new Document(reportPath);
                    DocumentBuilder builder = new DocumentBuilder(doc);

                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    FIPModel model = new FIPModel();
                    model = ds.getDataCommissioning(equipment, date, location, type, register);

                    doc.getRange().replace("equipment_data", model.getEquipment(),
                            true, true);
                    doc.getRange().replace("date_data", model.getDate(),
                            true, true);
                    doc.getRange().replace("location_data", model.getLocation(),
                            true, true);
                    doc.getRange().replace("type_data", model.getType(),
                            true, true);
                    doc.getRange().replace("register_data", model.getRegister(),
                            true, true);
                    doc.getRange().replace("client_data", model.getClient(),
                            true, true);

                    TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceID = telephonyManager.getDeviceId();

                    doc.getRange().replace("imei", "Imei : " + deviceID,
                            true, true);

                    PackageInfo pInfo;
                    try {
                        pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    doc.getRange().replace("version", "Version : " + version,
                            true, true);

                    doc.getRange().replace("UGMR", model.getDivision(),
                            true, true);
                    doc.getRange().replace("CSE", model.getContractor(),
                            true, true);
                    doc.getRange().replace("name_engineering", model.getNameEng(),
                            true, true);
                    doc.getRange().replace("name_maintenance", model.getNameMain(),
                            true, true);
                    doc.getRange().replace("name_areaowner", model.getNameAO(),
                            true, true);
                    doc.getRange().replace("name_ugmr", model.getNameUGMR(),
                            true, true);
                    doc.getRange().replace("Name_cse", model.getNameCSE(),
                            true, true);
                    doc.getRange().replace("name_head", model.getNameDept(),
                            true, true);

                    // REMARKS FOR GENERAL COMMISSIONING
                    doc.getRange().replace("Remarks1_1", model.getRemark1(),
                            true, true);
                    doc.getRange().replace("Remarks2_1", model.getRemark2(),
                            true, true);
                    doc.getRange().replace("Remarks3_1", model.getRemark3(),
                            true, true);
                    doc.getRange().replace("Remarks4_1", model.getRemark4(),
                            true, true);

                    // REMARKS FOR COMMISSIONING TEST
                    doc.getRange().replace("Remarks1", model.getRemark5(),
                            true, true);
                    doc.getRange().replace("Remarks2", model.getRemark6(),
                            true, true);
                    doc.getRange().replace("Remarks3", model.getRemark7(),
                            true, true);
                    doc.getRange().replace("Remarks4", model.getRemark8(),
                            true, true);
                    doc.getRange().replace("Remarks5", model.getRemark9(),
                            true, true);
                    doc.getRange().replace("Remarks6", model.getRemark10(),
                            true, true);
                    doc.getRange().replace("Remarks7", model.getRemark11(),
                            true, true);
                    doc.getRange().replace("Remarks8", model.getRemark12(),
                            true, true);
                    doc.getRange().replace("Remarks9", model.getRemark13(),
                            true, true);
                    doc.getRange().replace("Remarks10", model.getRemark14(),
                            true, true);
                    doc.getRange().replace("Remarks11", model.getRemark15(),
                            true, true);
                    doc.getRange().replace("Remarks12", model.getRemark16(),
                            true, true);

                    Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 0, true);
                    Row templateRow = (Row) sampleIdTable.getLastRow();
                    TextView columnTV;

                    // Multitogglebutton Reports
                    if (model.getQuestion1().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion1().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion2().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion2().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion3().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion3().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion4().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion4().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion5().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion5().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion6().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion6().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion7().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion7().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion8().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion8().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion9().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(5);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion9().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(5);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(5);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion10().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(6);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion10().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(6);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(6);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion11().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(7);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion11().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(7);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 1, true);
                        Row row = (Row) table.getRows().get(7);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion12().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(8);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion12().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(8);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(8);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion13().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(9);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion13().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(9);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(9);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion14().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(10);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion14().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(10);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(10);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion15().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(11);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion15().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(11);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(11);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion16().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(12);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion16().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(12);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row = (Row) table.getRows().get(12);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    // Dynamic Compilance Data Table
                    complianceData = ds.getAllComplianceEquipment(equipment, date, register);
                    if(complianceData.size() > 0) {
                        Table sampleIdTableCompliance = (Table) doc.getChild(
                                NodeType.TABLE, 4, true);

                        RowCollection rows = sampleIdTableCompliance.getRows();

                        Row currentRowCompliance = (Row) rows.get(1);
                        currentRowCompliance.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowCompliance = (Row) rows.get(1);
                        currentRowCompliance.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTableCompliance = (Table) doc.getChild(NodeType.TABLE, 4,
                                true);
                        rows = sampleIdTableCompliance.getRows();

                        for (int i = 0; i < complianceData.size(); i++) {
                            if (complianceData.get(i) != null) {
                                // Row currentRow;
                                if (i == 0) {
                                    currentRowCompliance = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowCompliance = (Row) rows.get(0);
                                    currentRowCompliance = (Row) currentRowCompliance
                                            .deepClone(true);
                                }
                                currentRowCompliance.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // No.
                                Cell cell = currentRowCompliance.getCells().get(0);

                                cell.getFirstParagraph().getRuns().clear();
                                cell.getCellFormat().getShading()
                                        .setBackgroundPatternColor(Color.WHITE);
                                cell.getFirstParagraph().appendChild(
                                        new Run(doc, String.valueOf(i + 1)));
                                for (Run run : cell.getFirstParagraph()
                                        .getRuns()) {
                                    // Set some font formatting properties
                                    Font font = run.getFont();
                                    font.setName("Arial");
                                    font.setSize(10);
                                    font.setColor(Color.BLACK);
                                }

                                // Finding
                                if (complianceData.get(i).getFindings()
                                        != null) {
                                    cell = currentRowCompliance.getCells().get(1);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, complianceData.get(i).getFindings()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Remarks
                                if (complianceData.get(i).getRemark()
                                        != null) {
                                    cell = currentRowCompliance.getCells().get(2);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, complianceData.get(i).getRemark()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Responsibility
                                if (complianceData.get(i).getResponsibility()
                                        != null) {
                                    cell = currentRowCompliance.getCells().get(3);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, complianceData.get(i).getResponsibility()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTableCompliance.getRows().add(currentRowCompliance);
                                }
                            }
                        }
                    }

                    // Dynamic Photo Data Table
                    photoData = ds.getAllPhotoData(equipment, date, register);
                    if(photoData.size() > 0) {
                        Table sampleIdTablePhoto = (Table) doc.getChild(
                                NodeType.TABLE, 5, true);

                        RowCollection rows = sampleIdTablePhoto.getRows();

                        Row currentRowPhoto = (Row) rows.get(1);
                        currentRowPhoto.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowPhoto = (Row) rows.get(1);
                        currentRowPhoto.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTablePhoto = (Table) doc.getChild(NodeType.TABLE, 5,
                                true);
                        rows = sampleIdTablePhoto.getRows();

                        for(int i = 0; i < photoData.size(); i++) {
                            if (photoData.get(i) != null) {
//						Row currentRow;
                                if (i == 0) {
                                    currentRowPhoto = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowPhoto = (Row) rows.get(0);
                                    currentRowPhoto = (Row) currentRowPhoto
                                            .deepClone(true);
                                }
                                currentRowPhoto.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // no.
                                Cell cell = currentRowPhoto.getCells().get(0);

                                cell.getFirstParagraph().getRuns().clear();
                                cell.getCellFormat()
                                        .getShading()
                                        .setBackgroundPatternColor(
                                                Color.WHITE);
                                cell.getFirstParagraph()
                                        .appendChild(
                                                new Run(doc, String
                                                        .valueOf(i + 1)));
                                for (Run run : cell.getFirstParagraph()
                                        .getRuns()) {
                                    // Set some font formatting properties
                                    Font font = run.getFont();
                                    font.setName("Arial");
                                    font.setSize(10);
                                    font.setColor(Color.BLACK);
                                }

                                // Photo
                                if (photoData.get(i)
                                        .getPathFile() != null) {
                                    cell = currentRowPhoto.getCells().get(1);

                                    cell.getFirstParagraph().getRuns()
                                            .clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);

                                    CellFormat format = cell
                                            .getCellFormat();
                                    double width = format.getWidth();
                                    builder.moveTo(cell.getFirstParagraph());
                                    // check image file
                                    if (Helper.getBitmapFromFile(photoData.get(i)
                                            .getPathFile()) != null) {
                                        // rotate image
                                        Bitmap bitmap = Helper
                                                .rotateImageAccordingToImageOrientation(
                                                        photoData
                                                                .get(i)
                                                                .getPathFile(),
                                                        (int) width);
                                        if (bitmap != null) {
                                            Shape image = builder
                                                    .insertImage(bitmap);

                                            double freePageWidth = width;

                                            // Is one of the sides of this
                                            // image too big for the
                                            // page?
                                            ImageSize size = image
                                                    .getImageData()
                                                    .getImageSize();
                                            boolean exceedsMaxPageSize = size
                                                    .getWidthPoints() > freePageWidth;

                                            if (exceedsMaxPageSize) {
                                                // Calculate the ratio to
                                                // fit the page size
                                                double ratio = freePageWidth
                                                        / size.getWidthPoints();

                                                Log.d("IMAGE",
                                                        "widthlonger : "
                                                                + ""
                                                                + " | ratio : "
                                                                + ratio);

                                                // Set the new size.
                                                image.setWidth(size
                                                        .getWidthPoints()
                                                        * ratio);
                                                image.setHeight(size
                                                        .getHeightPoints()
                                                        * ratio);
                                            }
                                        }
                                    }
                                }

                                // Title
                                if (photoData.get(i).getTitle()
                                        != null) {
                                    cell = currentRowPhoto.getCells().get(2);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, photoData.get(i).getTitle()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Title
                                if (photoData.get(i).getComment()
                                        != null) {
                                    cell = currentRowPhoto.getCells().get(3);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, photoData.get(i).getComment()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTablePhoto.getRows().add(currentRowPhoto);
                                }
                            }
                        }
                    }

//            sampleIdTable.getRows().removeAt(1);

                    // Engineering Signature
                    if (model.getSignEng() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 6, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignEng());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Maintenance Signature
                    if (model.getSignMain() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 6, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignMain());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Area Owner Signature
                    if (model.getSignAO() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 6, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(2);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignAO());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // UGMR Signature
                    if (model.getSignUGMR() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 7, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignUGMR());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // CSE Signature
                    if (model.getSignCSE() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 7, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignCSE());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Department Head Signature
                    if (model.getSignDept() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 7, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(2);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignDept());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // SAVE FILE
                    String fullPathName;
                    fullPathName = FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date());
                    if (!new File(fullPathName).exists()) {
                        new File(fullPathName).mkdirs();
                    }

                    if (!isGenerate) {
                        fullPathName += "/" + "Commissioning FIP" + model.getEquipment()
                                + model.getDate() + ".pdf";

                        doc.save(fullPathName);
                    } else {
                        fileName = new SimpleDateFormat("yyyyMMdd")
                                .format(new Date()) + " Commissioning FIP" + model.getEquipment();
                        fileName = Helper
                                .checkDuplicate(fileName, fullPathName, ".doc");

                        doc.save(fullPathName + "/" + fileName);
                    }

                    if (!isGenerate) {
                        File file = new File(fullPathName);
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String typeFile = "application/pdf";
                        intent.setDataAndType(Uri.fromFile(file), typeFile);
                        mActivity.startActivityForResult(intent,
                                OPEN_PREVIEW);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return  true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                Helper.dismissProgressDialog();
                if (!isGenerate) {
                    if (result) {

                    } else {

                    }
                } else if (isGenerate) {
                    if (result) {
                        Helper.showPopUpMessage(
                                mActivity,
                                "Generate Reports",
                                "Generate Reports success. File saved in : "
                                        + FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH,
                                null);
                    } else {
                        Helper.showPopUpMessage(mActivity, "Generate Reports",
                                "Generate Reports failed", null);
                    }
                }

            }
        }.execute();

    }


    public static void createHandOverReport(final Activity mActivity, final String equipment, final String date,
                                            final String location, final String type, final String register,
                                            final boolean isGenerate) {
        new AsyncTask<String, Integer, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Generate Handover FIP Reports. Please Wait...");
                } else if (!isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Preview Handover FIP Reports. Please Wait...");
                }
            }

            @Override
            protected Boolean doInBackground(String... params) {

                String reportPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();

                reportPath = FoldersFilesName.TEMPLATES_FOLDER_ON_EXTERNAL_PATH + "/" + FoldersFilesName.TEMPLATE_HANDOVER;

                String fileName;
                String formattedID = equipment;
                String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
                String docName = time + " Handover FIP " + formattedID;

                // CREATE IMAGE FOLDER
                String imageFolderPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/"
                        + FoldersFilesName.ROOT_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.APP_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.EXPORT_FOLDER_NAME + "/" + docName;
                File imageFolder = new File(imageFolderPath);

                try {
                    Document doc = new Document(reportPath);
                    DocumentBuilder builder = new DocumentBuilder(doc);

                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    HandoverModel model = new HandoverModel();
                    model = ds.getDataHandOver(equipment, date, location, type, register);

                    doc.getRange().replace("equipment_data", model.getEquipment(),
                            true, true);
                    doc.getRange().replace("date_data", model.getDate(),
                            true, true);
                    doc.getRange().replace("location_data", model.getLocation(),
                            true, true);
                    doc.getRange().replace("type_data", model.getType(),
                            true, true);
                    doc.getRange().replace("register_data", model.getRegister(),
                            true, true);
                    doc.getRange().replace("client_data", model.getClient(),
                            true, true);

                    TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceID = telephonyManager.getDeviceId();

                    doc.getRange().replace("imei", "Imei : " + deviceID,
                            true, true);

                    PackageInfo pInfo;
                    try {
                        pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    doc.getRange().replace("version", "Version : " + version,
                            true, true);

                    doc.getRange().replace("UGMR", model.getDivision(),
                            true, true);
                    doc.getRange().replace("CSE", model.getContractor(),
                            true, true);
                    doc.getRange().replace("name_engineering", model.getNameEng(),
                            true, true);
                    doc.getRange().replace("name_maintenance", model.getNameMain(),
                            true, true);
                    doc.getRange().replace("name_areaowner", model.getNameAO(),
                            true, true);
                    doc.getRange().replace("name_ugmr", model.getNameUGMR(),
                            true, true);
                    doc.getRange().replace("Name_cse", model.getNameCSE(),
                            true, true);
                    doc.getRange().replace("name_head", model.getNameDept(),
                            true, true);

                    termsData = ds.getAllTermsEquipment(equipment, date, register);
                    if(termsData.size() > 0) {
                        Table sampleIdTableTerms = (Table) doc.getChild(
                                NodeType.TABLE, 2, true);

                        RowCollection rows = sampleIdTableTerms.getRows();

                        Row currentRowTerms = (Row) rows.get(1);
                        currentRowTerms.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowTerms = (Row) rows.get(1);
                        currentRowTerms.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTableTerms = (Table) doc.getChild(NodeType.TABLE, 2,
                                true);
                        rows = sampleIdTableTerms.getRows();

                        for(int i = 0; i < termsData.size(); i++) {
                            if (termsData.get(i) != null) {
                                // Row currentRow;
                                if (i == 0) {
                                    currentRowTerms = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowTerms = (Row) rows.get(0);
                                    currentRowTerms = (Row) currentRowTerms
                                            .deepClone(true);
                                }
                                currentRowTerms.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // No.
                                Cell cell = currentRowTerms.getCells().get(0);

                                cell.getFirstParagraph().getRuns().clear();
                                cell.getCellFormat().getShading()
                                        .setBackgroundPatternColor(Color.WHITE);
                                cell.getFirstParagraph().appendChild(
                                        new Run(doc, String.valueOf(i + 1)));
                                for (Run run : cell.getFirstParagraph()
                                        .getRuns()) {
                                    // Set some font formatting properties
                                    Font font = run.getFont();
                                    font.setName("Arial");
                                    font.setSize(10);
                                    font.setColor(Color.BLACK);
                                }

                                // Finding
                                if (termsData.get(i).getTerms()
                                        != null) {
                                    cell = currentRowTerms.getCells().get(1);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, termsData.get(i).getTerms()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTableTerms.getRows().add(currentRowTerms);
                                }
                            }
                        }
                    }

                    // Engineering Signature
                    if (model.getSignEng() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignEng());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Maintenance Signature
                    if (model.getSignMain() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignMain());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Area Owner Signature
                    if (model.getSignAO() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 3, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(2);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignAO());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // UGMR Signature
                    if (model.getSignUGMR() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignUGMR());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // CSE Signature
                    if (model.getSignCSE() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignCSE());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // Department Head Signature
                    if (model.getSignDept() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(2);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSignDept());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // SAVE FILE
                    String fullPathName;
                    fullPathName = FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date());
                    if (!new File(fullPathName).exists()) {
                        new File(fullPathName).mkdirs();
                    }

                    if (!isGenerate) {
                        fullPathName += "/" + "Handover FIP " + model.getEquipment()
                                + model.getDate() + ".pdf";

                        doc.save(fullPathName);
                    } else {
                        fileName = new SimpleDateFormat("yyyyMMdd")
                                .format(new Date()) + " Handover FIP " + model.getEquipment();
                        fileName = Helper
                                .checkDuplicate(fileName, fullPathName, ".doc");

                        doc.save(fullPathName + "/" + fileName);
                    }

                    if (!isGenerate) {
                        File file = new File(fullPathName);
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String typeFile = "application/pdf";
                        intent.setDataAndType(Uri.fromFile(file), typeFile);
                        mActivity.startActivityForResult(intent,
                                OPEN_PREVIEW);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return  true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                Helper.dismissProgressDialog();
                if (!isGenerate) {
                    if (result) {

                    } else {

                    }
                } else if (isGenerate) {
                    if (result) {
                        Helper.showPopUpMessage(
                                mActivity,
                                "Generate Reports",
                                "Generate Reports success. File saved in : "
                                        + FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH,
                                null);
                    } else {
                        Helper.showPopUpMessage(mActivity, "Generate Reports",
                                "Generate Reports failed", null);
                    }
                }

            }
        }.execute();
    }

    public static void createChecklistReport(final Activity mActivity, final String date, final String location,
                                            final String company, final String system, final String from,
                                            final boolean isGenerate) {
        new AsyncTask<String, Integer, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Generate Checklist FIP Reports. Please Wait...");
                } else if (!isGenerate) {
                    Helper.showProgressDialog(mActivity, "Loading",
                            "Preview Checklist FIP Reports. Please Wait...");
                }
            }

            @Override
            protected Boolean doInBackground(String... params) {

                String reportPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();

                reportPath = FoldersFilesName.TEMPLATES_FOLDER_ON_EXTERNAL_PATH + "/" + FoldersFilesName.TEMPLATE_CHECKLIST_FIP;

                String fileName;
                String formattedID = location;
                String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
                String docName = time + " Checklist FIP " + formattedID;

                // CREATE IMAGE FOLDER
                String imageFolderPath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/"
                        + FoldersFilesName.ROOT_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.APP_FOLDER_NAME
                        + "/"
                        + FoldersFilesName.EXPORT_FOLDER_NAME + "/" + docName;
                File imageFolder = new File(imageFolderPath);

                try {
                    Document doc = new Document(reportPath);
                    DocumentBuilder builder = new DocumentBuilder(doc);

                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    FIPChecklistModel model = new FIPChecklistModel();
                    model = ds.getDataChecklist(date, location, company, system, from);

                    doc.getRange().replace("date_data", model.getDate(),
                            true, true);
                    doc.getRange().replace("location_data", model.getLocation(),
                            true, true);
                    doc.getRange().replace("company_data", model.getCompany(),
                            true, true);
                    doc.getRange().replace("system_data", model.getSystem(),
                            true, true);
                    doc.getRange().replace("from_data", model.getFrom(),
                            true, true);
                    doc.getRange().replace("no_data", model.getPages(),
                            true, true);
                    doc.getRange().replace("note_data", model.getNote(),
                            true, true);

                    TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
                    String deviceID = telephonyManager.getDeviceId();

                    doc.getRange().replace("imei", "Imei : " + deviceID,
                            true, true);

                    PackageInfo pInfo;
                    try {
                        pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    doc.getRange().replace("version", "Version : " + version,
                            true, true);

                    doc.getRange().replace("Dept_1", model.getDept_1(),
                            true, true);
                    doc.getRange().replace("Dept_2", model.getDept_2(),
                            true, true);
                    doc.getRange().replace("Dept_3", model.getDept_3(),
                            true, true);
                    doc.getRange().replace("Dept_4", model.getDept_4(),
                            true, true);
                    doc.getRange().replace("Name_1", model.getName_1(),
                            true, true);
                    doc.getRange().replace("Name_2", model.getName_2(),
                            true, true);
                    doc.getRange().replace("Name_3", model.getName_3(),
                            true, true);
                    doc.getRange().replace("Name_4", model.getName_3(),
                            true, true);

                    doc.getRange().replace("Remarks1", model.getRemark1(),
                            true, true);
                    doc.getRange().replace("Remarks2", model.getRemark2(),
                            true, true);
                    doc.getRange().replace("Remarks3", model.getRemark3(),
                            true, true);
                    doc.getRange().replace("Remarks4", model.getRemark4(),
                            true, true);

                    Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 0, true);
                    Row templateRow = (Row) sampleIdTable.getLastRow();
                    TextView columnTV;

                    // Multitogglebutton Reports
                    if (model.getQuestion1().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion1().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(1);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion2().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion2().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(2);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion3().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion3().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(3);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    if (model.getQuestion4().toString().equalsIgnoreCase("Pass")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(2);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else if (model.getQuestion4().toString().equalsIgnoreCase("No")) {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(3);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    } else {
                        Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                        Row row = (Row) table.getRows().get(4);
                        Cell dataCell = row.getCells().get(4);

                        dataCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        dataCell.getCellFormat().setVerticalAlignment(
                                CellVerticalAlignment.CENTER);
                        dataCell.getFirstParagraph().getRuns().clear();
                        dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                    }

                    inputData = ds.getAllInputChecklist(date, location, company);
                    if(inputData.size() > 0) {
                        Table sampleIdTableInput = (Table) doc.getChild(
                                NodeType.TABLE, 3, true);

                        RowCollection rows = sampleIdTableInput.getRows();

                        Row currentRowInput = (Row) rows.get(1);
                        currentRowInput.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowInput = (Row) rows.get(1);
                        currentRowInput.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTableInput = (Table) doc.getChild(NodeType.TABLE, 3,
                                true);
                        rows = sampleIdTableInput.getRows();

                        for(int i = 0; i < inputData.size(); i++) {
                            if (inputData.get(i) != null) {
                                // Row currentRow;
                                if (i == 0) {
                                    currentRowInput = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowInput = (Row) rows.get(0);
                                    currentRowInput = (Row) currentRowInput
                                            .deepClone(true);
                                }
                                currentRowInput.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // No.
                                Cell cell = currentRowInput.getCells().get(0);
                                Cell cellYes = currentRowInput.getCells().get(0);
                                Cell cellNo = currentRowInput.getCells().get(0);
                                Cell cellNA = currentRowInput.getCells().get(0);

                                // Device Input
                                if (inputData.get(i).getDevice()
                                        != null) {
                                    cell = currentRowInput.getCells().get(0);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, inputData.get(i).getDevice()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Description
                                if (inputData.get(i).getDesc()
                                        != null) {
                                    cell = currentRowInput.getCells().get(1);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, inputData.get(i).getDesc()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Pass or No or N/A
                                if (inputData.get(i).getResult()
                                        != null) {
                                    if(inputData.get(i).getResult().equalsIgnoreCase("Pass")) {
                                        cell = currentRowInput.getCells().get(2);
                                        cellNo = currentRowInput.getCells().get(3);
                                        cellNA = currentRowInput.getCells().get(4);


                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.LEFT);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellNo.getFirstParagraph().getRuns().clear();
                                        cellNA.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNo.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNA.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));
                                        for (Run run : cell.getFirstParagraph()
                                                .getRuns()) {
                                            // Set some font formatting
                                            // properties
                                            Font font = run.getFont();
                                            font.setName("Arial");
                                            font.setSize(10);
                                            font.setColor(Color.BLACK);
                                        }
                                    } else if (inputData.get(i).getResult().equalsIgnoreCase("No")) {
                                            cell = currentRowInput.getCells().get(3);
                                            cellYes = currentRowInput.getCells().get(2);
                                            cellNA = currentRowInput.getCells().get(4);

                                            cell.getFirstParagraph()
                                                    .getParagraphFormat()
                                                    .setAlignment(
                                                            ParagraphAlignment.CENTER);
                                            cell.getFirstParagraph().getRuns().clear();
                                            cellYes.getFirstParagraph().getRuns().clear();
                                            cellNA.getFirstParagraph().getRuns().clear();
                                            cell.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cellYes.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cellNA.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cell.getFirstParagraph().appendChild(
                                                    new Run(doc, passCheck));
                                        } else {
                                            cell = currentRowInput.getCells().get(4);
                                            cellYes = currentRowInput.getCells().get(2);
                                            cellNo = currentRowInput.getCells().get(3);

                                            cell.getFirstParagraph()
                                                    .getParagraphFormat()
                                                    .setAlignment(
                                                            ParagraphAlignment.CENTER);
                                            cell.getFirstParagraph().getRuns().clear();
                                            cellYes.getFirstParagraph().getRuns().clear();
                                            cellNo.getFirstParagraph().getRuns().clear();
                                            cell.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cellYes.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cellNo.getCellFormat()
                                                    .getShading()
                                                    .setBackgroundPatternColor(
                                                            Color.WHITE);
                                            cell.getFirstParagraph().appendChild(
                                                    new Run(doc, passCheck));

                                    }
                                }

                                // Remarks
                                if (inputData.get(i).getRemark()
                                        != null) {
                                    cell = currentRowInput.getCells().get(5);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, inputData.get(i).getRemark()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTableInput.getRows().add(currentRowInput);
                                }
                            }
                        }
                    }

                    outputData = ds.getAllOutputChecklist(date, location, company);
                    if(outputData.size() > 0) {
                        Table sampleIdTableOutput = (Table) doc.getChild(
                                NodeType.TABLE, 4, true);

                        RowCollection rows = sampleIdTableOutput.getRows();

                        Row currentRowOutput = (Row) rows.get(1);
                        currentRowOutput.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowOutput = (Row) rows.get(1);
                        currentRowOutput.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTableOutput = (Table) doc.getChild(NodeType.TABLE, 4,
                                true);
                        rows = sampleIdTableOutput.getRows();

                        for(int i = 0; i < outputData.size(); i++) {
                            if (outputData.get(i) != null) {
                                // Row currentRow;
                                if (i == 0) {
                                    currentRowOutput = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowOutput = (Row) rows.get(0);
                                    currentRowOutput = (Row) currentRowOutput
                                            .deepClone(true);
                                }
                                currentRowOutput.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // No.
                                Cell cell = currentRowOutput.getCells().get(0);
                                Cell cellYes = currentRowOutput.getCells().get(0);
                                Cell cellNo = currentRowOutput.getCells().get(0);
                                Cell cellNA = currentRowOutput.getCells().get(0);

                                // Device Input
                                if (outputData.get(i).getDevice()
                                        != null) {
                                    cell = currentRowOutput.getCells().get(0);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, outputData.get(i).getDevice()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Description
                                if (outputData.get(i).getDesc()
                                        != null) {
                                    cell = currentRowOutput.getCells().get(1);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, outputData.get(i).getDesc()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Pass or No or N/A
                                if (outputData.get(i).getResult()
                                        != null) {
                                    if(outputData.get(i).getResult().equalsIgnoreCase("Pass")) {
                                        cell = currentRowOutput.getCells().get(2);
                                        cellNo = currentRowOutput.getCells().get(3);
                                        cellNA = currentRowOutput.getCells().get(4);


                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.LEFT);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellNo.getFirstParagraph().getRuns().clear();
                                        cellNA.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNo.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNA.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));
                                        for (Run run : cell.getFirstParagraph()
                                                .getRuns()) {
                                            // Set some font formatting
                                            // properties
                                            Font font = run.getFont();
                                            font.setName("Arial");
                                            font.setSize(10);
                                            font.setColor(Color.BLACK);
                                        }
                                    } else if (outputData.get(i).getResult().equalsIgnoreCase("No")) {
                                        cell = currentRowOutput.getCells().get(3);
                                        cellYes = currentRowOutput.getCells().get(2);
                                        cellNA = currentRowOutput.getCells().get(4);

                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.CENTER);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellYes.getFirstParagraph().getRuns().clear();
                                        cellNA.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellYes.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNA.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));
                                    } else {
                                        cell = currentRowOutput.getCells().get(4);
                                        cellYes = currentRowOutput.getCells().get(2);
                                        cellNo = currentRowOutput.getCells().get(3);

                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.CENTER);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellYes.getFirstParagraph().getRuns().clear();
                                        cellNo.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellYes.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNo.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));

                                    }
                                }

                                // Remarks
                                if (outputData.get(i).getRemark()
                                        != null) {
                                    cell = currentRowOutput.getCells().get(5);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, outputData.get(i).getRemark()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTableOutput.getRows().add(currentRowOutput);
                                }
                            }
                        }
                    }

                    interlockData = ds.getAllInterlockChecklist(date, location, company);
                    if(interlockData.size() > 0) {
                        Table sampleIdTableInterlock = (Table) doc.getChild(
                                NodeType.TABLE, 5, true);

                        RowCollection rows = sampleIdTableInterlock.getRows();

                        Row currentRowInterlock = (Row) rows.get(1);
                        currentRowInterlock.getRowFormat()
                                .setAllowBreakAcrossPages(true);

                        currentRowInterlock = (Row) rows.get(1);
                        currentRowInterlock.getRowFormat().setAllowBreakAcrossPages(true);
                        sampleIdTableInterlock = (Table) doc.getChild(NodeType.TABLE, 5,
                                true);
                        rows = sampleIdTableInterlock.getRows();

                        for(int i = 0; i < interlockData.size(); i++) {
                            if (interlockData.get(i) != null) {
                                // Row currentRow;
                                if (i == 0) {
                                    currentRowInterlock = (Row) rows.get(i + 1);
                                } else {
                                    // add new row
                                    currentRowInterlock = (Row) rows.get(0);
                                    currentRowInterlock = (Row) currentRowInterlock
                                            .deepClone(true);
                                }
                                currentRowInterlock.getRowFormat()
                                        .setAllowBreakAcrossPages(true);

                                // No.
                                Cell cell = currentRowInterlock.getCells().get(0);
                                Cell cellYes = currentRowInterlock.getCells().get(0);
                                Cell cellNo = currentRowInterlock.getCells().get(0);
                                Cell cellNA = currentRowInterlock.getCells().get(0);

                                // Device Input
                                if (interlockData.get(i).getDevice()
                                        != null) {
                                    cell = currentRowInterlock.getCells().get(0);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, interlockData.get(i).getDevice()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Description
                                if (interlockData.get(i).getDesc()
                                        != null) {
                                    cell = currentRowInterlock.getCells().get(1);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, interlockData.get(i).getDesc()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                // Pass or No or N/A
                                if (interlockData.get(i).getResult()
                                        != null) {
                                    if(interlockData.get(i).getResult().equalsIgnoreCase("Pass")) {
                                        cell = currentRowInterlock.getCells().get(2);
                                        cellNo = currentRowInterlock.getCells().get(3);
                                        cellNA = currentRowInterlock.getCells().get(4);


                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.LEFT);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellNo.getFirstParagraph().getRuns().clear();
                                        cellNA.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNo.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNA.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));
                                        for (Run run : cell.getFirstParagraph()
                                                .getRuns()) {
                                            // Set some font formatting
                                            // properties
                                            Font font = run.getFont();
                                            font.setName("Arial");
                                            font.setSize(10);
                                            font.setColor(Color.BLACK);
                                        }
                                    } else if (interlockData.get(i).getResult().equalsIgnoreCase("No")) {
                                        cell = currentRowInterlock.getCells().get(3);
                                        cellYes = currentRowInterlock.getCells().get(2);
                                        cellNA = currentRowInterlock.getCells().get(4);

                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.CENTER);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellYes.getFirstParagraph().getRuns().clear();
                                        cellNA.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellYes.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNA.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));
                                    } else {
                                        cell = currentRowInterlock.getCells().get(4);
                                        cellYes = currentRowInterlock.getCells().get(2);
                                        cellNo = currentRowInterlock.getCells().get(3);

                                        cell.getFirstParagraph()
                                                .getParagraphFormat()
                                                .setAlignment(
                                                        ParagraphAlignment.CENTER);
                                        cell.getFirstParagraph().getRuns().clear();
                                        cellYes.getFirstParagraph().getRuns().clear();
                                        cellNo.getFirstParagraph().getRuns().clear();
                                        cell.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellYes.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cellNo.getCellFormat()
                                                .getShading()
                                                .setBackgroundPatternColor(
                                                        Color.WHITE);
                                        cell.getFirstParagraph().appendChild(
                                                new Run(doc, passCheck));

                                    }
                                }

                                // Remarks
                                if (interlockData.get(i).getRemark()
                                        != null) {
                                    cell = currentRowInterlock.getCells().get(5);

                                    cell.getFirstParagraph()
                                            .getParagraphFormat()
                                            .setAlignment(
                                                    ParagraphAlignment.LEFT);
                                    cell.getFirstParagraph().getRuns().clear();
                                    cell.getCellFormat()
                                            .getShading()
                                            .setBackgroundPatternColor(
                                                    Color.WHITE);
                                    cell.getFirstParagraph().appendChild(
                                            new Run(doc, interlockData.get(i).getRemark()));
                                    for (Run run : cell.getFirstParagraph()
                                            .getRuns()) {
                                        // Set some font formatting
                                        // properties
                                        Font font = run.getFont();
                                        font.setName("Arial");
                                        font.setSize(10);
                                        font.setColor(Color.BLACK);
                                    }
                                }

                                if (i > 0) {
                                    sampleIdTableInterlock.getRows().add(currentRowInterlock);
                                }
                            }
                        }
                    }

                    // 1 Signature
                    if (model.getSign_1() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 6, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSign_1());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // 2 Signature
                    if (model.getSign_2() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 6, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSign_2());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // 3 Signature
                    if (model.getSign_3() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 7, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(0);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSign_3());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // 4 Signature
                    if (model.getSign_4() != null) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 7, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(1);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(model
                                .getSign_4());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    // SAVE FILE
                    String fullPathName;
                    fullPathName = FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date());
                    if (!new File(fullPathName).exists()) {
                        new File(fullPathName).mkdirs();
                    }

                    if (!isGenerate) {
                        fullPathName += "/" + "Checklist FIP" + model.getLocation()
                                + model.getDate() + ".pdf";

                        doc.save(fullPathName);
                    } else {
                        fileName = new SimpleDateFormat("yyyyMMdd")
                                .format(new Date()) + " Checklist FIP" + model.getLocation();
                        fileName = Helper
                                .checkDuplicate(fileName, fullPathName, ".doc");

                        doc.save(fullPathName + "/" + fileName);
                    }

                    if (!isGenerate) {
                        File file = new File(fullPathName);
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String typeFile = "application/pdf";
                        intent.setDataAndType(Uri.fromFile(file), typeFile);
                        mActivity.startActivityForResult(intent,
                                OPEN_PREVIEW);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return  true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                Helper.dismissProgressDialog();
                if (!isGenerate) {
                    if (result) {

                    } else {

                    }
                } else if (isGenerate) {
                    if (result) {
                        Helper.showPopUpMessage(
                                mActivity,
                                "Generate Reports",
                                "Generate Reports success. File saved in : "
                                        + FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH,
                                null);
                    } else {
                        Helper.showPopUpMessage(mActivity, "Generate Reports",
                                "Generate Reports failed", null);
                    }
                }

            }
        }.execute();
    }
}
