package com.ptfi.commfip.Utils;

/**
 * Created by senaardyputra on 7/19/16.
 */
public class LanguageConstants {

    public String TOGGLE_BTN = "Indonesia";
    public String Q1 = "Instalasi selesai";
    public String Q2 = "Memakai bahan standar FSME";
    public String Q3 = "Test fungsional";
    public String Q4 = "Ada penggambaran";
    public String Q5 = "Kondisi FIP Panel";
    public String Q6 = "Koneksi dan kabel";
    public String Q7 = "Koneksi dan sumber energi";
    public String Q8 = "Batre";
    public String Q9 = "Detektor";
    public String Q10 = "Aktuator Manual";
    public String Q11 = "Batalkan switch";
    public String Q12 = "Klakson dan lampu";
    public String Q13 = "Tanda evakuasi";
    public String Q14 = "Jangan masukan tana";
    public String Q15 = "Menyambungkan";
    public String Q16 = "Pengenal dan karet penutup";

    public LanguageConstants(boolean isEnglish) {
        if (isEnglish) {
             TOGGLE_BTN = "English";
             Q1 = "Installation complete";
             Q2 = "Material use meet FSME standard";
             Q3 = "Function test";
             Q4 = "Drawing available";
             Q5 = "FIP Panel condition";
             Q6 = "Wiring and connections";
             Q7 = "Power supply and connection";
             Q8 = "Batteries";
             Q9 = "Detectors";
             Q10 = "Manual actuators";
             Q11 = "Abort switches";
             Q12 = "Horns and strobes";
             Q13 = "Evacuation signs";
             Q14 = "Do not enter signs";
             Q15 = "Interlock system";
             Q16 = "Tags and seals";
        } else {
            TOGGLE_BTN = "Indonesia";
            Q1 = "Instalasi selesai";
            Q2 = "Memakai bahan standar FSME";
            Q3 = "Test fungsional";
            Q4 = "Ada penggambaran";
            Q5 = "Kondisi FIP Panel";
            Q6 = "Koneksi dan kabel";
            Q7 = "Koneksi dan sumber energi";
            Q8 = "Batre";
            Q9 = "Detektor";
            Q10 = "Aktuator Manual";
            Q11 = "Batalkan switch";
            Q12 = "Klakson dan lampu";
            Q13 = "Tanda evakuasi";
            Q14 = "Jangan masukan tana";
            Q15 = "Menyambungkan";
            Q16 = "Pengenal dan karet penutup";
        }
    }
}
