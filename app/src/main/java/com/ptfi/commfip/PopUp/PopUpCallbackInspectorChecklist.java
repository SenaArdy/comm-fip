package com.ptfi.commfip.PopUp;

import com.ptfi.commfip.Models.FIPChecklistModel;
import com.ptfi.commfip.Models.FIPModel;

/**
 * Created by senaardyputra on 8/2/16.
 */
public interface PopUpCallbackInspectorChecklist {

    public void onDataSave(FIPChecklistModel model, String name, String division);
}
