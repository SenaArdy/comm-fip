package com.ptfi.commfip.PopUp;

import com.ptfi.commfip.Models.FIPModel;
import com.ptfi.commfip.Models.HandoverModel;

/**
 * Created by senaardyputra on 7/27/16.
 */
public interface PopUpCallbackInspector {
    public void onDataSave(FIPModel model, String name, String division);

    public void onDataSaveHO(HandoverModel model, String name, String division);
}
