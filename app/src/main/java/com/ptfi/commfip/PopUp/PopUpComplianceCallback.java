package com.ptfi.commfip.PopUp;

import com.ptfi.commfip.Models.ComplianceModel;

/**
 * Created by senaardyputra on 6/18/16.
 */
public interface PopUpComplianceCallback {
    public void onDataSave(ComplianceModel model);
}
