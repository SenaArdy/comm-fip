package com.ptfi.commfip.PopUp;

import com.ptfi.commfip.Models.PhotoModel;

/**
 * Created by senaardyputra on 7/18/16.
 */
public interface PopUpPhotosCallback {
    public void onDataSave(PhotoModel model);
}
