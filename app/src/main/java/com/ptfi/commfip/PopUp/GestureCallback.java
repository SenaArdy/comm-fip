package com.ptfi.commfip.PopUp;

import android.graphics.Bitmap;

/**
 * Created by senaardyputra on 6/20/16.
 */
public interface GestureCallback {
    public void onBitmapSaved(Bitmap bitmap);
}
