package com.ptfi.commfip.PopUp;

import com.ptfi.commfip.Models.ChecklistModel;
import com.ptfi.commfip.Models.FIPModel;

/**
 * Created by senaardyputra on 8/2/16.
 */
public interface PopUpCallbackChecklist {

    public void onDataSaveInput(ChecklistModel model);

    public void onDataSaveOutput(ChecklistModel model);

    public void onDataSaveInterlock(ChecklistModel model);
}
