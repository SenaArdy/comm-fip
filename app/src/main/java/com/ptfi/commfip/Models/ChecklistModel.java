package com.ptfi.commfip.Models;

/**
 * Created by senaardyputra on 8/2/16.
 */
public class ChecklistModel {

    public long id;
    public String date;
    public String location;
    public String company;
    public String device;
    public String desc;
    public String result;
    public String remark;

    public ChecklistModel() {
        this.id = -1;
        this.date = "";
        this.location = "";
        this.company = "";
        this.device = "";
        this.desc = "";
        this.result = "";
        this.remark = "";
    }

    public ChecklistModel(long id, String date, String location, String company, String device, String desc, String result, String remark) {
        this.id = id;
        this.date = date;
        this.location = location;
        this.company = company;
        this.device = device;
        this.desc = desc;
        this.result = result;
        this.remark = remark;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
