package com.ptfi.commfip.Models;

/**
 * Created by senaardyputra on 8/2/16.
 */
public class FIPChecklistModel {

    public long id;
    public String date;
    public String location;
    public String company;
    public String system;
    public String from;
    public String pages;
    public String question1;
    public String remark1;
    public String question2;
    public String remark2;
    public String question3;
    public String remark3;
    public String question4;
    public String remark4;
    public String note;
    public String name_1;
    public String id_1;
    public byte[]  sign_1;
    public String dept_1;
    public String name_2;
    public String id_2;
    public byte[]  sign_2;
    public String dept_2;
    public String name_3;
    public String id_3;
    public byte[]  sign_3;
    public String dept_3;
    public String name_4;
    public String id_4;
    public byte[]  sign_4;
    public String dept_4;

    public FIPChecklistModel() {
        this.id = -1;
        this.date = "";
        this.location = "";
        this.company = "";
        this.system = "";
        this.from = "";
        this.pages = "";
        this.question1 = "";
        this.remark1 = "";
        this.question2 = "";
        this.remark2 = "";
        this.question3 = "";
        this.remark3 = "";
        this.question4 = "";
        this.remark4 = "";
        this.note = "";
        this.name_1 = "";
        this.id_1 = "";
        this.sign_1 = null;
        this.dept_1 = "";
        this.name_2 = "";
        this.id_2 = "";
        this.sign_2 = null;
        this.dept_2 = "";
        this.name_3 = "";
        this.id_3 = "";
        this.sign_3 = null;
        this.dept_3 = "";
        this.name_4 = "";
        this.id_4 = "";
        this.sign_4 = null;
        this.dept_4 = "";
    }

    public FIPChecklistModel(long id, String date, String location, String company, String system, String from, String pages, String question1, String remark1,
                             String question2, String remark2, String question3, String remark3, String question4, String remark4, String note,
                             String name_1, String id_1, byte[]  sign_1, String dept_1, String name_2, String id_2, byte[]  sign_2, String dept_2, String name_3,
                             String id_3, byte[]  sign_3, String dept_3, String name_4, String id_4, byte[]  sign_4, String dept_4) {
        this.id = id;
        this.date = date;
        this.location = location;
        this.company = company;
        this.system = system;
        this.from = from;
        this.pages = pages;
        this.question1 = question1;
        this.remark1 = remark1;
        this.question2 = question2;
        this.remark2 = remark2;
        this.question3 = question3;
        this.remark3 = remark3;
        this.question4 = question4;
        this.remark4 = remark4;
        this.note = note;
        this.name_1 = name_1;
        this.id_1 = id_1;
        this.sign_1 = sign_1;
        this.dept_1 = dept_1;
        this.name_2 = name_2;
        this.id_2 = id_2;
        this.sign_2 = sign_2;
        this.dept_2 = dept_2;
        this.name_3 = name_3;
        this.id_3 = id_3;
        this.sign_3 = sign_3;
        this.dept_3 = dept_3;
        this.name_4 = name_4;
        this.id_4 = id_4;
        this.sign_4 = sign_4;
        this.dept_4 = dept_4;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public String getQuestion4() {
        return question4;
    }

    public void setQuestion4(String question4) {
        this.question4 = question4;
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName_1() {
        return name_1;
    }

    public void setName_1(String name_1) {
        this.name_1 = name_1;
    }

    public String getId_1() {
        return id_1;
    }

    public void setId_1(String id_1) {
        this.id_1 = id_1;
    }

    public byte[]  getSign_1() {
        return sign_1;
    }

    public void setSign_1(byte[]  sign_1) {
        this.sign_1 = sign_1;
    }

    public String getDept_1() {
        return dept_1;
    }

    public void setDept_1(String dept_1) {
        this.dept_1 = dept_1;
    }

    public String getName_2() {
        return name_2;
    }

    public void setName_2(String name_2) {
        this.name_2 = name_2;
    }

    public String getId_2() {
        return id_2;
    }

    public void setId_2(String id_2) {
        this.id_2 = id_2;
    }

    public byte[]  getSign_2() {
        return sign_2;
    }

    public void setSign_2(byte[]  sign_2) {
        this.sign_2 = sign_2;
    }

    public String getDept_2() {
        return dept_2;
    }

    public void setDept_2(String dept_2) {
        this.dept_2 = dept_2;
    }

    public String getName_3() {
        return name_3;
    }

    public void setName_3(String name_3) {
        this.name_3 = name_3;
    }

    public String getId_3() {
        return id_3;
    }

    public void setId_3(String id_3) {
        this.id_3 = id_3;
    }

    public byte[]  getSign_3() {
        return sign_3;
    }

    public void setSign_3(byte[]  sign_3) {
        this.sign_3 = sign_3;
    }

    public String getDept_3() {
        return dept_3;
    }

    public void setDept_3(String dept_3) {
        this.dept_3 = dept_3;
    }

    public String getName_4() {
        return name_4;
    }

    public void setName_4(String name_4) {
        this.name_4 = name_4;
    }

    public String getId_4() {
        return id_4;
    }

    public void setId_4(String id_4) {
        this.id_4 = id_4;
    }

    public byte[]  getSign_4() {
        return sign_4;
    }

    public void setSign_4(byte[]  sign_4) {
        this.sign_4 = sign_4;
    }

    public String getDept_4() {
        return dept_4;
    }

    public void setDept_4(String dept_4) {
        this.dept_4 = dept_4;
    }
}
