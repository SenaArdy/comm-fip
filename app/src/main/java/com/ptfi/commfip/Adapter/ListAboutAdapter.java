package com.ptfi.commfip.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ptfi.commfip.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 5/11/16.
 */
public class ListAboutAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<String[]> listAboutContent;

    public ListAboutAdapter(Activity activity,
                            ArrayList<String[]> listAboutContent) {
        super();
        this.activity = activity;
        this.listAboutContent = listAboutContent;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listAboutContent.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listAboutContent.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_about, null);

        TextView titleTV = (TextView) convertView.findViewById(R.id.title);
        TextView contentTV = (TextView) convertView.findViewById(R.id.content);

        String[] listAbout = listAboutContent.get(position);
        titleTV.setText(listAbout[0]);
        contentTV.setText(listAbout[1]);

        return convertView;
    }
}
