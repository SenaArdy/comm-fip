package com.ptfi.commfip.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptfi.commfip.Models.FIPChecklistModel;
import com.ptfi.commfip.Models.FIPModel;
import com.ptfi.commfip.PopUp.GeneratePopUp;
import com.ptfi.commfip.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 8/5/16.
 */
public class ListGenerateDataChecklistFIP extends RecyclerView.Adapter<ListGenerateDataChecklistFIP.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<FIPChecklistModel> checklistData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView dateTV;
        TextView locationTV;
        TextView companyTV;
        TextView systemTV;
        TextView fromTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            this.locationTV = (TextView) itemView.findViewById(R.id.locationTV);
            this.companyTV = (TextView) itemView.findViewById(R.id.companyTV);
            this.systemTV = (TextView) itemView.findViewById(R.id.systemTV);
            this.fromTV = (TextView) itemView.findViewById(R.id.fromTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String date = checklistData.get(position).getDate();
            String location = checklistData.get(position).getLocation();
            String company = checklistData.get(position).getCompany();
            String system = checklistData.get(position).getSystem();
            String from = checklistData.get(position).getFrom();

            GeneratePopUp.checklistFIPPopUp(mActivity, date, location, company, system, from);
        }
    }

    public ListGenerateDataChecklistFIP(Activity mActivity, ArrayList<FIPChecklistModel> checklistData) {
        super();
        this.mActivity = mActivity;
        this.checklistData = checklistData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_generate_data_checklist, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView dateTV = holder.dateTV;
        TextView locationTV = holder.locationTV;
        TextView companyTV = holder.companyTV;
        TextView systemTV = holder.systemTV;
        TextView fromTV = holder.fromTV;

        dateTV.setText("Date : " + checklistData.get(position).getDate());
        locationTV.setText(checklistData.get(position).getLocation());
        companyTV.setText(checklistData.get(position).getCompany());
        systemTV.setText("System : " + checklistData.get(position).getSystem());
        fromTV.setText("From : " + checklistData.get(position).getFrom());
    }

    @Override
    public int getItemCount() {
        return checklistData.size();
    }
}
