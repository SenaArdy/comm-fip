package com.ptfi.commfip.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.commfip.Database.DataSource;
import com.ptfi.commfip.Fragments.FIPChecklistFragment;
import com.ptfi.commfip.Models.ChecklistModel;
import com.ptfi.commfip.PopUp.PopUp;
import com.ptfi.commfip.PopUp.PopUpCallbackChecklist;
import com.ptfi.commfip.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 8/2/16.
 */
public class ListAdapterOutput extends RecyclerView.Adapter<ListAdapterOutput.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<ChecklistModel> checklistData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView deviceTV;
        TextView remarkTV;
        TextView descTV;
        ImageView statusIV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.deviceTV = (TextView) itemView.findViewById(R.id.deviceTV);
            this.remarkTV = (TextView) itemView.findViewById(R.id.remarkTV);
            this.descTV = (TextView) itemView.findViewById(R.id.descTV);
            this.statusIV = (ImageView) itemView.findViewById(R.id.statusIV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String date = checklistData.get(position).getDate();
            String location = checklistData.get(position).getLocation();
            String company = checklistData.get(position).getCompany();
            final String device = checklistData.get(position).getDevice();
            final String desc = checklistData.get(position).getDesc();
            final String result = checklistData.get(position).getResult();
            final String remark = checklistData.get(position).getRemark();
            PopUp.checklistIns deviceType = PopUp.checklistIns.output;

            PopUp.PopUpChecklist(mActivity, deviceType, date, location, company, new PopUpCallbackChecklist() {
                @Override
                public void onDataSaveInput(ChecklistModel model) {

                }

                @Override
                public void onDataSaveOutput(ChecklistModel model) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    ds.renewOutputData(model, device, desc, result, remark);
                    FIPChecklistFragment.outputRV();

                    ds.close();
                }

                @Override
                public void onDataSaveInterlock(ChecklistModel model) {

                }
            });
        }
    }

    public ListAdapterOutput(Activity mActivity, ArrayList<ChecklistModel> checklistData) {
        super();
        this.mActivity = mActivity;
        this.checklistData = checklistData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_checklist, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView deviceTV = holder.deviceTV;
        TextView remarkTV = holder.remarkTV;
        TextView descTV = holder.descTV;
        ImageView statusIV = holder.statusIV;

        deviceTV.setText(checklistData.get(position).getDevice());
        remarkTV.setText("Remark : " + checklistData.get(position).getRemark());
        descTV.setText("Description : " + checklistData.get(position).getDesc());
        if (checklistData.get(position).getResult().equalsIgnoreCase("Pass")) {
            statusIV.setBackground(mActivity.getResources().getDrawable(R.drawable.done_icons));
        } else if (checklistData.get(position).getResult().equalsIgnoreCase("No")) {
            statusIV.setBackground(mActivity.getResources().getDrawable(R.drawable.not_done_icons));
        } else {
            statusIV.setBackground(mActivity.getResources().getDrawable(R.drawable.na_icon));
        }
    }

    @Override
    public int getItemCount() {
        return checklistData.size();
    }
}
