package com.ptfi.commfip.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commfip.Adapter.ListAdapterInput;
import com.ptfi.commfip.Adapter.ListAdapterInterlock;
import com.ptfi.commfip.Adapter.ListAdapterOutput;
import com.ptfi.commfip.Adapter.ListComplianceAdapter;
import com.ptfi.commfip.Adapter.ListPhotoAdapter;
import com.ptfi.commfip.Database.DataSource;
import com.ptfi.commfip.Models.ChecklistModel;
import com.ptfi.commfip.Models.ComplianceModel;
import com.ptfi.commfip.Models.DataSingleton;
import com.ptfi.commfip.Models.FIPChecklistModel;
import com.ptfi.commfip.Models.InspectorModel;
import com.ptfi.commfip.Models.PhotoModel;
import com.ptfi.commfip.PopUp.PopUp;
import com.ptfi.commfip.PopUp.PopUpCallbackChecklist;
import com.ptfi.commfip.PopUp.PopUpCallbackInspectorChecklist;
import com.ptfi.commfip.R;
import com.ptfi.commfip.Utils.FoldersFilesName;
import com.ptfi.commfip.Utils.Helper;
import com.ptfi.commfip.Utils.Reports;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by senaardyputra on 8/2/16.
 */
public class FIPChecklistFragment extends Fragment {

    protected static FragmentActivity mActivity;

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static final int REQUEST_SCAN_BARCODE_ENG_CHK = 248;
    static final int REQUEST_SCAN_BARCODE_MAIN_CHK = 249;

    private PopUp.page pages;
    private PopUp.signature signatures;

    ArrayList<InspectorModel> dataInspector;

    static String inspectorIDFalse;

    static EditText dateET, locationET, companyET, systemET, fromET, pagesET, noteET, sign1ET, sign2ET, sign3ET, sign4ET,  remark1ET, remark2ET, remark3ET, remark4ET;
    static TextInputLayout dateTL, locationTL, companyTL, systemTL, fromTL, pagesTL, noteTL, sign1TL, sign2TL, sign3TL, sign4TL, remark1TL, remark2TL, remark3TL, remark4TL;
    static TextView Q80TV, Q90TV, Q100TV;

    static MultiStateToggleButton insp1, insp2, insp3, insp4;

    TextView imeiCode, versionCode;

    static ImageView addInputIV, addOutputIV, addInterlockSystemIV;

    static CardView generateCV, previewCV;

    static RecyclerView outputRV, inputRV, interlockRV;

    static RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager outputLayoutManager;
    RecyclerView.LayoutManager interlockLayoutManager;

    private static ArrayList<ChecklistModel> inputData = new ArrayList<>();
    private static ArrayList<ChecklistModel> outputData = new ArrayList<>();
    private static ArrayList<ChecklistModel> interlockData = new ArrayList<>();

    static ListAdapterInput adapterInput;
    static ListAdapterOutput adapterOutput;
    static ListAdapterInterlock adapterInterlock;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fip_checklist, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PopUp.pageComm = PopUp.page.checklist;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        initComponent(rootView);

        return rootView;
    }

    public void initComponent(final View rootView) {
        if (rootView != null) {
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            locationET = (EditText) rootView.findViewById(R.id.locationEqET);
            companyET = (EditText) rootView.findViewById(R.id.companyET);
            systemET = (EditText) rootView.findViewById(R.id.systemET);
            fromET = (EditText) rootView.findViewById(R.id.fromET);
            pagesET = (EditText) rootView.findViewById(R.id.pagesET);
            noteET= (EditText) rootView.findViewById(R.id.noteET);
            sign1ET = (EditText) rootView.findViewById(R.id.sign1ET);
            sign2ET = (EditText) rootView.findViewById(R.id.sign2ET);
            sign3ET = (EditText) rootView.findViewById(R.id.sign3ET);
            sign4ET = (EditText) rootView.findViewById(R.id.sign4ET);

            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            locationTL = (TextInputLayout) rootView.findViewById(R.id.locationTL);
            companyTL = (TextInputLayout) rootView.findViewById(R.id.companyTL);
            systemTL = (TextInputLayout) rootView.findViewById(R.id.systemTL);
            fromTL = (TextInputLayout) rootView.findViewById(R.id.fromTL);
            pagesTL = (TextInputLayout) rootView.findViewById(R.id.pagesTL);
            noteTL = (TextInputLayout) rootView.findViewById(R.id.noteTL);
            sign1TL = (TextInputLayout) rootView.findViewById(R.id.sign1TL);
            sign2TL = (TextInputLayout) rootView.findViewById(R.id.sign2TL);
            sign3TL = (TextInputLayout) rootView.findViewById(R.id.sign3TL);
            sign4TL = (TextInputLayout) rootView.findViewById(R.id.sign4TL);

            insp1 = (MultiStateToggleButton) rootView.findViewById(R.id.insp1);
            insp2 = (MultiStateToggleButton) rootView.findViewById(R.id.insp2);
            insp3 = (MultiStateToggleButton) rootView.findViewById(R.id.insp3);
            insp4 = (MultiStateToggleButton) rootView.findViewById(R.id.insp4);

            remark1ET = (EditText) rootView.findViewById(R.id.remark1ET);
            remark2ET = (EditText) rootView.findViewById(R.id.remark2ET);
            remark3ET = (EditText) rootView.findViewById(R.id.remark3ET);
            remark4ET = (EditText) rootView.findViewById(R.id.remark4ET);

            remark1TL = (TextInputLayout) rootView.findViewById(R.id.remark1TL);
            remark2TL = (TextInputLayout) rootView.findViewById(R.id.remark2TL);
            remark3TL = (TextInputLayout) rootView.findViewById(R.id.remark3TL);
            remark4TL = (TextInputLayout) rootView.findViewById(R.id.remark4TL);

            inputRV = (RecyclerView) rootView.findViewById(R.id.inputRV);
            outputRV = (RecyclerView) rootView.findViewById(R.id.outputRV);
            interlockRV = (RecyclerView) rootView.findViewById(R.id.interlockRV);

            addInputIV = (ImageView) rootView.findViewById(R.id.addInputIV);
            addInputIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.checklistIns pages = PopUp.checklistIns.input;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertInput(model);
                                            inputRV();

                                            ds.close();
                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {

                                        }
                                    });
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.checklistIns pages = PopUp.checklistIns.input;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertInput(model);
                                            inputRV();

                                            ds.close();
                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            addOutputIV = (ImageView) rootView.findViewById(R.id.addOutputIV);
            addOutputIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.checklistIns pages = PopUp.checklistIns.output;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertOutput(model);
                                            outputRV();


                                            ds.close();
                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {

                                        }
                                    });
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.checklistIns pages = PopUp.checklistIns.output;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertOutput(model);
                                            outputRV();


                                            ds.close();
                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {

                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            addInterlockSystemIV = (ImageView) rootView.findViewById(R.id.addInterlockSystemIV);
            addInterlockSystemIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.checklistIns pages = PopUp.checklistIns.interlock;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertInterlock(model);
                                            interlockRV();


                                            ds.close();
                                        }
                                    });

                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.checklistIns pages = PopUp.checklistIns.interlock;
                            PopUp.PopUpChecklist(mActivity, pages, dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString(),
                                    new PopUpCallbackChecklist() {
                                        @Override
                                        public void onDataSaveInput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveOutput(ChecklistModel model) {

                                        }

                                        @Override
                                        public void onDataSaveInterlock(ChecklistModel model) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.insertInterlock(model);
                                            interlockRV();


                                            ds.close();
                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            generateCV = (CardView) rootView.findViewById(R.id.generateCV);
            generateCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateGenerate()){
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if(ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            Reports.createChecklistReport(mActivity, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), systemET.getText().toString(), fromET.getText().toString(), true);
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            Reports.createChecklistReport(mActivity, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), systemET.getText().toString(), fromET.getText().toString(), true);
                        }
                        ds.close();
                    }
                }
            });

            previewCV = (CardView) rootView.findViewById(R.id.previewCV);
            previewCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateGenerate()){
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if(ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            Reports.createChecklistReport(mActivity, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), systemET.getText().toString(), fromET.getText().toString(), false);
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            Reports.createChecklistReport(mActivity, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), systemET.getText().toString(), fromET.getText().toString(), false);
                        }
                        ds.close();
                    }
                }
            });

            Q80TV = (TextView) rootView.findViewById(R.id.Q80TV);
            Q80TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (inputRV.getVisibility() == View.VISIBLE) {
                        inputRV.setVisibility(View.GONE);
                    } else if (inputRV.getVisibility() == View.GONE) {
                        inputRV.setVisibility(View.VISIBLE);
                    }
                }
            });

            Q90TV = (TextView) rootView.findViewById(R.id.Q90TV);
            Q90TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (outputRV.getVisibility() == View.VISIBLE) {
                        outputRV.setVisibility(View.GONE);
                    } else if (outputRV.getVisibility() == View.GONE) {
                        outputRV.setVisibility(View.VISIBLE);
                    }
                }
            });

            Q100TV = (TextView) rootView.findViewById(R.id.Q100TV);
            Q100TV .setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (interlockRV.getVisibility() == View.VISIBLE) {
                        interlockRV.setVisibility(View.GONE);
                    } else if (interlockRV.getVisibility() == View.GONE) {
                        interlockRV.setVisibility(View.VISIBLE);
                    }
                }
            });

            sign1ET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.checklist;
                            signatures = PopUp.signature.name1;
                            scanBar(signatures);
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.checklist;
                            signatures = PopUp.signature.name1;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            sign2ET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.checklist;
                            signatures = PopUp.signature.name2;
                            scanBar(signatures);
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.checklist;
                            signatures = PopUp.signature.name2;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            sign3ET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.checklist;
                            PopUp.signature signatures = PopUp.signature.name3;
                            PopUp.showInspectorPopUpChecklist(mActivity, null, null, null,  pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                                        @Override
                                        public void onDataSave(FIPChecklistModel model, String name, String division) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.updateSign3(model);
                                            sign3ET.setText(name);
                                            sign3TL.setHint(division);

                                            ds.close();
                                        }
                                    });
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.checklist;
                            PopUp.signature signatures = PopUp.signature.name3;
                            PopUp.showInspectorPopUpChecklist(mActivity, null, null, null, pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                                        @Override
                                        public void onDataSave(FIPChecklistModel model, String name, String division) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.updateSign3(model);
                                            sign3ET.setText(name);
                                            sign3TL.setHint(division);

                                            ds.close();
                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            sign4ET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateAdd()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.checklist;
                            PopUp.signature signatures = PopUp.signature.name4;
                            PopUp.showInspectorPopUpChecklist(mActivity, null, null, null, pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                                        @Override
                                        public void onDataSave(FIPChecklistModel model, String name, String division) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.updateSign4(model);
                                            sign4ET.setText(name);
                                            sign4TL.setHint(division);

                                            ds.close();
                                        }
                                    });
                        } else if (!ds.validateDataChecklist(dateET.getText().toString(), locationET.getText().toString(), companyET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.checklist;
                            PopUp.signature signatures = PopUp.signature.name4;
                            PopUp.showInspectorPopUpChecklist(mActivity, null, null, null, pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                                    companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                                        @Override
                                        public void onDataSave(FIPChecklistModel model, String name, String division) {
                                            DataSource ds = new DataSource(mActivity);
                                            ds.open();

                                            ds.updateSign4(model);
                                            sign4ET.setText(name);
                                            sign4TL.setHint(division);

                                            ds.close();
                                        }
                                    });
                        }
                        ds.close();
                    }
                }
            });

            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());
        }

    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
                case name1:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG_CHK);
                    break;

                case name2:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN_CHK);
                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    public static void inputRV() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        inputRV.setLayoutManager(layoutManager);
        inputRV.setItemAnimator(new DefaultItemAnimator());

        inputData = ds.getAllInputChecklist(dateET.getText().toString(),
                locationET.getText().toString(), companyET.getText().toString());

        adapterInput = new ListAdapterInput(mActivity, inputData);
        inputRV.setAdapter(adapterInput);
        ds.close();
    }

    public static void outputRV() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        outputRV.setLayoutManager(layoutManager);
        outputRV.setItemAnimator(new DefaultItemAnimator());

        outputData = ds.getAllOutputChecklist(dateET.getText().toString(),
                locationET.getText().toString(), companyET.getText().toString());

        adapterOutput = new ListAdapterOutput(mActivity, outputData);
        outputRV.setAdapter(adapterOutput);
        ds.close();
    }

    public static void interlockRV() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(mActivity);
        interlockRV.setLayoutManager(layoutManager);
        interlockRV.setItemAnimator(new DefaultItemAnimator());

        interlockData = ds.getAllInterlockChecklist(dateET.getText().toString(),
                locationET.getText().toString(), companyET.getText().toString());

        adapterInterlock = new ListAdapterInterlock(mActivity, interlockData);
        interlockRV.setAdapter(adapterInterlock);
        ds.close();
    }

    public static boolean validateAdd() {
        boolean status =  true;

        if(dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill date first");
        }

        if(locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill location first");
        }

        if(companyET.getText().toString().length() == 0) {
            status = false;
            companyTL.setErrorEnabled(true);
            companyTL.setError("*Please fill company first");
        }

        if(systemET.getText().toString().length() == 0) {
            status = false;
            systemTL.setErrorEnabled(true);
            systemTL.setError("*Please fill system first");
        }

        if(fromET.getText().toString().length() == 0) {
            status = false;
            fromTL.setErrorEnabled(true);
            fromTL.setError("*Please fill from first");
        }

        if(pagesET.getText().toString().length() == 0) {
            status = false;
            pagesTL.setErrorEnabled(true);
            pagesTL.setError("*Please fill no of pages first");
        }

        return status;
    }

    public static boolean validateGenerate() {
        boolean status =  true;

        if(dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill date first");
        }

        if(locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill location first");
        }

        if(companyET.getText().toString().length() == 0) {
            status = false;
            companyTL.setErrorEnabled(true);
            companyTL.setError("*Please fill company first");
        }

        if(systemET.getText().toString().length() == 0) {
            status = false;
            systemTL.setErrorEnabled(true);
            systemTL.setError("*Please fill system first");
        }

        if(fromET.getText().toString().length() == 0) {
            status = false;
            fromTL.setErrorEnabled(true);
            fromTL.setError("*Please fill from first");
        }

        if(pagesET.getText().toString().length() == 0) {
            status = false;
            pagesTL.setErrorEnabled(true);
            pagesTL.setError("*Please fill no of pages first");
        }

        if(sign1ET.getText().toString().length() == 0) {
            status = false;
            sign1TL.setErrorEnabled(true);
            sign1TL.setError("*Please fill Sign first");
        }

        if(sign2ET.getText().toString().length() == 0) {
            status = false;
            sign2TL.setErrorEnabled(true);
            sign2TL.setError("*Please fill Sign first");
        }

        if(sign3ET.getText().toString().length() == 0) {
            status = false;
            sign3TL.setErrorEnabled(true);
            sign3TL.setError("*Please fill Sign first");
        }

        if(sign4ET.getText().toString().length() == 0) {
            status = false;
            sign4TL.setErrorEnabled(true);
            sign4TL.setError("*Please fill Sign first");
        }

        if(insp1.getValue() == 1) {
            if (remark1ET.getText().toString().length() == 0) {
                status = false;
                remark1TL.setErrorEnabled(true);
                remark1TL.setError("* Please fill remarks if you choose 'No'");
            }
        } else if (insp1.getValue() == -1) {
            status = false;
            remark1TL.setErrorEnabled(true);
            remark1TL.setError("* Please choose of one choice");
        }

        if(insp2.getValue() == 1) {
            if (remark2ET.getText().toString().length() == 0) {
                status = false;
                remark2TL.setErrorEnabled(true);
                remark2TL.setError("* Please fill remarks if you choose 'No'");
            }
        } else if (insp2.getValue() == -1) {
            status = false;
            remark2TL.setErrorEnabled(true);
            remark2TL.setError("* Please choose of one choice");
        }

        if(insp3.getValue() == 1) {
            if (remark3ET.getText().toString().length() == 0) {
                status = false;
                remark3TL.setErrorEnabled(true);
                remark3TL.setError("* Please fill remarks if you choose 'No'");
            }
        } else if (insp3.getValue() == -1) {
            status = false;
            remark3TL.setErrorEnabled(true);
            remark3TL.setError("* Please choose of one choice");
        }

        if(insp4.getValue() == 1) {
            if (remark4ET.getText().toString().length() == 0) {
                status = false;
                remark4TL.setErrorEnabled(true);
                remark4TL.setError("* Please fill remarks if you choose 'No'");
            }
        } else if (insp4.getValue() == -1) {
            status = false;
            remark4TL.setErrorEnabled(true);
            remark4TL.setError("* Please choose of one choice");
        }

        return status;
    }

    public static void datePickers(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }


    public static void saveData(){
        FIPChecklistModel model = new FIPChecklistModel();

        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setCompany(companyET.getText().toString());
        model.setSystem(systemET.getText().toString());
        model.setFrom(fromET.getText().toString());
        model.setPages(pagesET.getText().toString());
        model.setNote(noteET.getText().toString());

        if(insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if(insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }

        if(insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if(insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }

        if(insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if(insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }

        if(insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if(insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        DataSource ds = new DataSource(mActivity);
        ds.insertChecklistFIP(model);
        ds.close();
    }

    public static void updateData(){
        FIPChecklistModel model = new FIPChecklistModel();

        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setCompany(companyET.getText().toString());
        model.setSystem(systemET.getText().toString());
        model.setFrom(fromET.getText().toString());
        model.setPages(pagesET.getText().toString());
        model.setNote(noteET.getText().toString());

        if(insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if(insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }

        if(insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if(insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }

        if(insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if(insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }

        if(insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if(insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        DataSource ds = new DataSource(mActivity);
        ds.updateDataChecklist(model);
        ds.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SCAN_BARCODE_ENG_CHK
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            sign1ET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                pages = PopUp.page.checklist;
                signatures = PopUp.signature.name1;
                PopUp.showInspectorPopUpChecklist(mActivity, "FSME Engineering", inspectorID, name, pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                        companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                            @Override
                            public void onDataSave(FIPChecklistModel model, String name, String division) {
                                DataSource ds = new DataSource(mActivity);
                                ds.open();

                                ds.updateSign1(model);

                                ds.close();
                            }
                        });
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_ENG_CHK
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN_CHK
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            sign2ET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                pages = PopUp.page.checklist;
                signatures = PopUp.signature.name2;
                PopUp.showInspectorPopUpChecklist(mActivity, "FSME Maintenance", inspectorID, name, pages, signatures, dateET.getText().toString(), locationET.getText().toString(),
                        companyET.getText().toString(), new PopUpCallbackInspectorChecklist() {
                            @Override
                            public void onDataSave(FIPChecklistModel model, String name, String division) {
                                DataSource ds = new DataSource(mActivity);
                                ds.open();

                                ds.updateSign2(model);

                                ds.close();
                            }
                        });
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN_CHK
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == Reports.OPEN_PREVIEW) {
            File fileName = new File(
                    FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/"
                            + new SimpleDateFormat("yyyyMMdd")
                            .format(new Date()) + "/"
                            + "Checklist Sprinkler.pdf");
            if (fileName.exists())
                fileName.delete();
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning FIP Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("SAVE AND EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validateGenerate()) {
                    saveData();
                    mActivity.finish();
                }
            }
        });

        builder.show();
    }
}
